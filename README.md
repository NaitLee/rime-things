# rime-things

Some custom & practical [RIME](https://rime.im/)/[Trime](https://github.com/osfans/trime)/[OpenCC](https://github.com/BYVoid/OpenCC) configurations.

## Things

[How to apply a patch](#applying-a-patch)

### `trime-wider-candidate.yaml`

By default Trime have very small gaps between input candidates on smart phones, causing mistakes & anger frequently.

Apply this patch to `rime/trime.custom.yaml` (in Android shared storage) to increase the candidate width. Feel free to adjust the values inside.

### `switches-plus.yaml`

Relying on the fact that RIME have OpenCC, this patch will leverage **a lot of** character conversion capabilities via a switch (accessible via F4 schema menu)

<details>

<summary>Click to expand detailed introduction</summary>

- default 預設: no conversion, input as-is from dictionary

  RIME default dictionaries are in OpenCC traditional characters, which have some characters in more traditional form than TW/HK variants.

- zh_hans 简体字: convert input to CN simplified characters

  This is useful for daily life in China Mainland.

- zh_hant 傳統字: convert input to OpenCC traditional characters

  Explicitly convert inputted characters to OpenCC traditional ones. Useful for:

  - Knowing about what’s a character’s “true” traditional form
  - Learning these gradually for ones who used to simplified characters
  - Automatically correct you if you’re likely to input “hybrid” characters  
    (e.g. you’re used to simplified characters and trying to use shape-based schema to input traditional ones)

- zh_hant_tw 正體字: convert input to TW variation of traditional characters

  Useful for daily life in Taiwan.

- zh_hant_twp 正體字/臺灣用詞: convert input to TW variation of traditional characters, along with phrase convention matched

  Useful for Mainland people to communicate with friends from Taiwan, especially in topic of Information Technology — different terms are converted accordingly.

- zh_hans_p 简体字/大陆用词: convert input to CN simplified characters, along with phrase convention matched

  Useful for Taiwan people to communicate with friends from Mainland, especially in topic of Information Technology — different terms are converted accordingly.

- zh_hant_hk 香港字: convert input to HK variant of traditional characters

  Useful for daily life in Hong Kong.

- zh_han_jp 日本新字: convert input to JP variant of characters (Shinjitai)

  This one seems weird, but powers you to input characters in Japanese convention without much knowledge of the entire language. Good for looking up a Japanese dictionary.

Conversion tips are enabled to see what’s being converted.

</details>

Since this is applied per-schema, you need all desired schema’s `.custom.yaml` have this patch’s content included.

Relative OpenCC configurations are in `opencc/`. They need to be copied to rime data directory for this one to work. They can also be used for other purpose.

Among them, `t2sp.json` is a homemade configuration (which isn’t in original OpenCC); `patch-*.txt` are included in relative configurations for patching phrase conversion further.

## License

Consider that official (T)Rime configurations are licensed under various licenses (some are Copyleft), and OpenCC is licensed under Apache 2.0, these things are also licensed under Apache License 2.0.

## Applying a patch

Example scenarios:

1. You have Luna Pinyin schema `luna_pinyin.schema.yaml` in use, but didn’t customize it. In this case just copy the patch to configuration directory as `luna_pinyin.custom.yaml`. Re-deploy RIME to make it work.

2. You also have Combo Pinyin `combo_pinyin.schema.yaml` in use, but customized it for including a [extended dictionary](https://github.com/rime/home/wiki/DictionaryPack). So you have the file:

```yaml
# combo_pinyin.custom.yaml
patch:
  "translator/dictionary": extended
```

  In this case, just append the patch to this file, **without repeating the “`patch:`”**


```yaml
# combo_pinyin.custom.yaml
patch:
  "translator/dictionary": extended
  # your patch below
  switches:
    # ...
  zh_hans:
    opencc_config: t2s.json
    option_name: zh_hans
    tips: all
    # ...
```

  Save the file and re-deploy to make it work.
